# داکرسازیدن برکاهای توزیع‌شده

یکی از روش‌های کاهش بار بر سرور، استفاده از معماری توزیع‌شده است؛ برای نمونه، جداییدن سرور فایل از سرور برکا.

با داکر هم می‌توان یه پروژه را به‌شکل توزیعیده داکرسازید. چون این نیازمند اجرای دستورهای زیادی توسط داکر CLI است، از داکرکامپوز می‌استفیم تا از اجرای دستورهای تکراری پرهزیده شود.

## داکرکامپوز (Docker Compose)

برای خودکاریدن ساخت و اجرای یک یا چند کانتینر از این ابزار می‌استفیم. نخست باید یه فایل به نام `docker-compose.yml` با ساختار کلی و نحو زیر بایجادیم:

```yml
# Define a version of docker compose to be used (REQUIRED)
version: "3"

# Here's what we want docker to do
services:
  # Define the service (the name of services are optional)
  redis-service:
    # Define the base image
    image: "redis"
  node-app:
    # Define the Dockerfile
    build: .
    # To map local ports to container
    ports:
      # <local_port>:<container_port>
      - "8081:8081"
```

**توجه.** تمام سرویس‌های تعریفیده در یه `docker-compose.yml` به‌شکل خودکار در یه شبکه قرار می‌گیرند؛ و از هر کانتیر در این شبکه، می‌توان با استفاده از اسم سرویس به هر کانتیر دیگر در این شبکه دسترسی داشت.

## اجراییدن/توقفاندن سرویس‌ها

پس از تعریف `docker-compose.yml` برای اجراییدن/توقفاندن تمام سرویس‌ها کافیه بنا به نیاز یکی از دستورهای زیر را در مسیر `docker-compose.yml` باجراییم:

```shell
# To run services
docker-compose up
#OR, to run a group of container in the background
docker-compose up -d

# To rebuild the images that are listed inside the docker-compose.yml
docker-compose up --build

# To stop services
docker-compose down

# To get status of containers (on appropriate directory)
docker-compose ps
```

## بازآغازیدن سرویس‌ها بنابر سیاست‌هایی

بنابر یکی از سیاست‌های زیر می‌توان چگونگی بازآغازیدن یه کانتینر را هنگام توقفیدن‌اش تعیینید.

**سیاست‌های بازآغازیدن (Restart Policies):**

<div dir="rtl" align="right" markdown="1">

- no: هرگز برای بازآغازیدن تلاش نشود. (پیشفرض)
- always: اگر به هر دلیلی کانتینراِ توقفیده شد، برای بازآغازیدن‌اش تلاش شود.
- on-failure: تنها هنگامی کانتینراِ بازآغازیده شود که با یه کد خطا توقفیده شده باشد.
- unless-stopped: همیشه برای بازآغازیدن کانتیراِ تلاش شود، مگر دستور توقف توسط داکر داده شود.

</div>

نحو اعلانیدن در داکرکامپوز:

```yml
version: "3"

services:
  redis-service:
    image: "redis"
  node-app:
    build: .
    restart: always
    ports:
      - "8081:8081"
```
