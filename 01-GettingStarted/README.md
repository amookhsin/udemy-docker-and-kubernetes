# شروع کار

## چرخه‌وجودی کانتینر داکر

زیچرخه‌ی یه کانتینر شامل مرحله‌های زیر است:

- ایجادیدن: در این مرحله کانتینر برپایه یه ایمیج ایجاد می‌شود؛
- اجراییدن: در این مرحله در کانتینر یه دستور اجرا می‌شود؛
- خاموشیدن: در این مرحله کانتینر در وضعیت خاموش قرار می‌گیرد.

**نکته:** دستور اجرایی یه کانتینر خاموش را نمی‌توان تغییراند.

```shell
# Creating the container
docker create <image name>
# Starting the container
docker start -a <image ID> # -a: attaching output of container to current terminal

# Creating a container and starting it in single command
docker run <image name>
# Overriding Default Command
docker run <image name> <command>
```

## دستورهای پایه

```shell
# PS: Listing running containers
docker ps
# Listing all executed containers
docker ps -a # --all

# Removing containers/images
docker system prune     # remove all images and containers

# Log: retrieving the output of container
docker logs <container ID>

# To stop safely the running containers by issuing SIGTERM
docker stop <container ID>
# To stop immediately the running containers by issuing SIGKILL
```

## اجراییدن یه دستور در یه کانتینر

پارامترها:

<div dir="rtl" align="right" markdown="1">

- -i: اتصال جریان ورودی خط‌فرمان سیستم‌عامل میزبان به داکر؛
- -t: نمودن دقیق پایانه‌ی درون کانتیر روی سیستم‌عامل میزبان.

</div>

```shell
docker run --rm -it <conteiner_id> <command>
# Execute an additional command in a running container
docker exec -it <container_id> <command>

# Example
docker run --rm -it ubuntu sh
docker exec -it 4e7157638a20 sh
```

## مفهوم‌ها

- کانتینر (container): شامل منابع سخت‌افزاری (پردازنده، کارت شبکه، ذخیره‌گاه، و ...) است که برای یه نرم‌افزار خاص توسط هسته‌ی سیستم‌عامل ایجاد می‌شود.
- ایمیج (image): یه برگرفت (snapshot) از یه کانتینر است.
