# روندکار ساختن یه محصول-رتبه

گام‌های ساختن یه نرم‌افزار محصول-رتبه به روش درهمش پایسته (Continuous integration):

- ساختن دو داکرفایل متفاوت برای محیط‌های توسعه و استقرار
- استفیدن از یه سرویس برای دهمیدن پایسته مانند TravisCI

## پیکریدن TravisCI

از این سرویس برای آزمودن نرم‌افزار پس از هر بار push کردن کدهایمان در مخزن درو و نیز درصورت تایید آزمون‌ها برپاییدن نرم‌افزار در محیط کسبکار می‌استفیم.

### روندکار

روندکار پس از ایجاد مخزن دور و پیونداندش با حساب TravisCI:

1. پیکریدن TravisCI: تعیینیدن کارهایی که باید پس از هر push انجامیده شود در فایل `.travis.yml` در ریشه‌ی پروژه. ساختار کلی دستورهایمان در `.travis.yml`:

   - اعلامیدن نیاز به استفیدن داکر
   - درخواست ساختن ایمج‌امان از `Dockerfile.dev`
   - اعلامیدن چگونگی اجرایدن آزمون‌ها
   - اعلامیدن چگونگی برپایاندن کدهایمان در محیط کسبکار

   **نحو پیکریدن TravisCI.**

   <div dir="ltr" align="left" markdown="1">

   ```yml
   # To tell TravisCI we need superuser level permissions
   sudo: required

   # To tell services that we need the docker preinstalled
   services:
     - docker

   # To define a series of command that we want executed before are ran
   before_install:
     - docker build -t hakhsin/frontend -f Dockerfile.dev .

   # To tell travisCi how to run our test suite
   script:
     - docker run hakhsin/frontend npm run test -- --coverage

    # To tell TravisCi how to take our application and deploy it of to AWS
    deploy:
      provider: elasticbeanstalk
      region: ""              # Comm from app url in elasticbeanstalk
      app: "app-name"         # The name that we use for created the app in elasticbeanstalk
      env: "App-name-env"     # Take it from elasticbeanstalk (The name after app-name in your dashboard)
      bucket_name: ""         # In services tab in aws search for s3 then find it from the list
      bucket_path: "app-name" # The exact name with your app name
      on:
        branch: master
      # In aws in services tab search for iam then create a user in Users section
      # So define the info's user as variables in TravisCI variables
      access_key_id: $AWS_ACCESS_KEY
      secret_access_key:
        - secret: "$AWS_SECRET_KEY"
   ```

   </div>

## پیوست

### استفیدن از داکرفایل با نام سفارشی

<div dir="ltr" align="left" markdown="1">

در داکر cli:

```shell
docker build -f Dockerfile.dev .
```

در docker-compose:

```yml
# ...
build:
  context: .
  dockerfile: Dockerfile.dev
# ...
```

</div>

### پیوستاندن پوشه‌ی محلی به کانتینر

برای پیوستاندن پوشه‌ی پروژه محلی با کانتینر، از مفهوم volume می‌استفیم:

![مفهوم volume](assets/volume.png)

**تعیین volume در داکر cli:**

![-v option](assets/option-v.png)

```shell
docker run -v "<local_path>:<container_path>"
```

**توجه.** هنگامیکه بخواهیم به پوشه‌ی موجود در کانتینر، مانند `node_module`، که در پوشه‌ی محلی وجود ندارد را حفظ کنیم، باید مانند `-v "/app/node_module"` پوشه‌ی محلی را نتعینیم.

**تعیین volume در داکر `docker-compose.yml`**

از کلید `volumes` همانند زیر می‌استفیم:

```yml
#...
volumes:
  - "/app/node_module"
  - ".:/app/"
#...
```

### آزمودن برنامه

دو رویکرد برای اجرای آزمون‌های برنامه وجود دارد:

- استفیدن از `docker exec -it <container_id> npm run test`: پیچیده ولی تعاملی.
- تعریف دو سرویس جدا در `docker-compose.yml`: ساده ولی ناتعاملی.

  <div dir="ltr" align="left" markdown="1">

  ```yml
  version: "3"

  services:
    web:
      build:
        context: .
        dockerfile: Dockerfile.dev
      ports:
        - "4001:3000"
      volumes:
        - "/app/node_modules"
        - ".:/app"
    tests:
      build:
        context: .
        dockerfile: Dockerfile.dev
      volumes:
        - "/app/node_modules"
        - ".:/app"
      command: ["npm", "run", "test"]
  ```

  </div>

### پیوستاندن پایانه محلی به کانتینر در اجرا

برای پیوستاندن stdin، stdout، و stderr پایانه محلی به فرایند اصلی یه کانتینر از دستور زیر می‌استفیم:

```shell
docker attach <container_id>
```

### ساخت چندگامی (Multi-step build)

گاهی نیازه یه کانتینر (به‌ویژه استقرار) در چند گام ساخته شود تا نیازمندی‌های موقت در کانتینر نهایی باقی نماند؛ مانند هنگامیکه می‌خواهیم خروجی پروژه‌امان را در محیط استقرار باجراییم. برای پیادن ساخت چندگامی همانند زیر می‌عملیم:

```Dockerfile
# Build Phase
FROM node:alpine AS builder

WORKDIR /app

COPY package.json .
RUN npm install

COPY . .

RUN npm run build

# RUN Phase
FROM nginx

# To being map automatically directly by elasticbeanstalk and
# To communication with developer to understand the properly port map
EXPOSE 80

# To copy the staff from different phase
COPY --from=builder /app/build /usr/share/nginx/html
```

**اجراییدن:**

```shell
docker run -p 4001:80 <the_image_id>
```
