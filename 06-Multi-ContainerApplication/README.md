# ساختن برکای چند-سرویسی

فرانمای کار اینه که با استفیدن از docker-compose کانتینرهای برکایمان را باهم می‌شبکیم.

![معماری برکایمان](assets/app_diagram.png)

**قرارداد.** از نام‌های `Dockerfile` و `Dockerfile.dev` برای ساختن ایمیج‌های محیط توسعه و استقرار باستفیم.

**نکته.** هنگام داکرسازیدن محیط استقرار می‌توان ایمیح برکای پیشا را با nginx ادغامید یا از یه nginx را در ایمیج پیشا نیز بنصبیم.

## داکر-کامپوز (`docker-compose.yml`)

## تعینیدن ایمیج

تعینیدن یه ایمیج دور (در یه مخزن دور):

```yml
version: "3"

services:
    # ...
    postgres:
        image: 'postgres:latest'
    # ...
```

 تعینیدن یه فایل‌داکر:

```yml
version: "3"

services:
    # ...
    server:
        build:
            # To define the dockerfile name:
            - dockerfile: Dockerfile.dev
            # To define the path of the dockerfile
            - context: ./server
    # ...
```

### تعریفیدن متغیرهای محلی

نحوها تعریف یه متغیر محلی در یه سرویس در زمان اجرا:

<div dir="rtl" align="right" markdown="1">

* `variable`: با مقدار متغیر‌اِ در سیستم‌عامل میزبان
* `variable=value`: با مقدار مشخص‌شده

</div>

```yml
# ...
worker:
    # ...
    environment:
      - REDIS_HOST=redis
      - REDIS_PORT=6379
```
