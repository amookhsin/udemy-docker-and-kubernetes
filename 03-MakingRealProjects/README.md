# داکرسازیدن پروژه واقعی

گام‌های داکرسازیدن یه پروژه‌ی واقعی:

1. برگزیدن ایمیج مناسب
2. اشتراکیدن پروژه بین ماشین میزبان و کانتینر
3. نصبیدن وابستگی‌ها
4. تعیین دستور آغازین
5. مسیریدن ترافیک‌های یه پورت خاص میزبان محلی به کانتینر هنگام اجرایدن ایمیج‌اِ

**مثال.** برای پروژه‌ی `simpleweb`:

```dockerfile
# 1. Specify a base image
FROM node:14.17-alpine

# 2. Copy project on container
# COPY <path_to_project_on_local> <path_to_dis_on_container>
COPY ./ ./

# 3. Install some dependency
RUN npm install

# 4. Default command
CMD ["npm", "start"]
```

و در آخر هنگام اجرای ایمجی:

```shell
docker build -t <docker_id>/<project_name>:<tag> <path_to_dockerfile>
docker run -p <local_port>:<container_port> <image_id>
```

## کمیدن بازاجرای فرمان

چون هرگاه پودمانی از پروژه تغییر کند، فرمان‌های پس از <span dir="ltr" markdown="1">`COPY ./ ./`</span> در `Dockerfile` هنگام اجرای دستور ساختن کانتینر (`docker build`) بازاجرایده می‌شوند، بهتره برای پرهیز از بازنصبیدن بی‌مورد وابستگی‌ها پودمان‌های پروژه‌امان را در دو مرحله به درون کانتینر کپی کنیم:

```dockerfile
FROM node:14.17-alpine

# Install some dependency
COPY ./package.json ./
RUN npm install

# Copy project on container
COPY ./ ./

CMD ["npm", "start"]
```

## جداییدن فضای‌کاری

بهتره برای پرهیز از تداخل بین پوشه‌های پروژه با پوشه‌های سیستم‌عامل، یه فضای‌کاری در کانتینر تعیین کنیم:

```dockerfile
FROM node:14.17-alpine

# Define work directory
WORKDIR /user/app

# ...
```
